﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : AsteroidsModule.GameController
{
    public Text LazerCountText;
    public Player Player2D;
    public Player Player3D;
    public GameObject Object3D;
    public GameObject Object2D;
    static bool Is2D = true;
    public Toggle Is2DToggle;
    public Text ScoreText;
    // Start is called before the first frame update
    protected new void Start()
    {
        
        Controller = this;
        Object3D.SetActive(false);
        Object2D.SetActive(false);
        if (Is2D)
        {
            Object2D.SetActive(true);
        }
        else
        {
            Object3D.SetActive(true);
        }

        if (Is2D)
            Player = Player2D;
        else
            Player = Player3D;
        base.Start();
    }

    public override void PlayerDead()
    {
        base.PlayerDead();
       
        Is2DToggle.isOn = Is2D;
    }
    public void ChangeIs2D(Toggle toggle)
    {
        Is2D = toggle.isOn;
    }
    

    public override void UpdateLazerCount(int lazerCount)
    {
        LazerCountText.text = new string('/', lazerCount);
    }

    public override void AddScore(int scoreAdd)
    {
        Score += scoreAdd;
        ScoreText.text = "Score:" + Score;
    }
}
